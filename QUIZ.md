
# OH WEEKLY ASSESSMENT 2
  
In this assessment, you have to answer some basic question about Internet. You can find the answer from the internet or by discussing with your friend, but make sure you understand what you are doing. Please deliver this assessment on time no matter what results you make! Thank you.
  
Deadline: Saturday, 28 December 2021, 23:59 WIB

## Instructions
- Clone this project repository
- Try to complete complete the task
- Create git repository using `weekly-assessment-2` as the name of project
- Push your result to it
- Fill this form when you are finish: https://docs.google.com/forms/d/e/1FAIpQLSfe-kDIu-efXh_62tq3dAl3DwuN9PB2Pae0ywYPKSMUiM0sOw/viewform

## Task Requirements
- [ ] Complete: Answer all Question
  
# Question:
- Explain How the Internet Works.
- What's the meaning of communication protocol
- Mention HTTP status codes
- Make Analogy Regarding how Front end and Backend communicate each other
- Mention What method in HTTP protocol
  
  # Answer

  1. THE INTERNET WORKS  BY CONNECTING A HUGE NUMBER OF COMPUTERS ALL ACROSS THE WORLD IN AN INTERACTIVE NETWORK THAT ALLOWS THEM TO SEND AND RECEIVE PACKETS OF INFORMATION. 

“Internet bekerja dengan menghubujngkan sejumlah besar komputerdi seluruh dunia dalam jaringan interaktif yang memungkinkan mereka mengirim dan menerima paket informasi.”

  2.	A COMMUNICATION PROTOCOL IS A SYSTEM OF RULES THAT ALLOWS TWO OR MORE ENTITIES OF A COMMUNICATIONS SYSTEM TO TRANSMIT INFORMATION VIA ANY KIND OF VARIATION A PHYSICAL QUANTITY.
“protocol komunikasi adalah system aturan yang mengizinkan dua atau lebih entitas system komunikasi untuk mengirim informasi melalui jenis variasi kuantitas fisik”

3.	HTTP status codes.
    - A. 1xx : information
    - B. 2xx : success
    - C. 3xx : redirection
    - D. 4xx : client error
    - E. 5xx : server error

4. analogy regarding how frontend and bakend communicate each other
    - Kitchen as back-end n Table as Front-end n API as waiter, database as icebox
    - Tabel is user interface like url:http://mysite.com
    - menu book like HTML n CSS because what user wants will be on the menu book
    - user requests menu n waiter send to Kitchen
    - in kitchen proces requests
    - can use icebox if data/material you need
    - requests finish n waiter sent to user.

5. HTTP protocol
  -	GET : untuk meminta data dari sumber daya tertentu 
  -	POST : untuk mengirim data ke server , dan server membuat / memperbarui sumber daya 
  -	PUT : untuk mengirim data ke server untuk membuat / memperbarui sumber daya bersifat idempoten 
  -	HEAD : hampir identik dengan GET, tetapi tanpa body response 
  -	DELETE : untuk menghapus sumber daya yang ditentukan 
  -	OPTIONS : untuk menjelaskan opsi komunikasi untuk sumber daya target
